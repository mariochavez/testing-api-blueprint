FORMAT: 1A
HOST: https://api-test.com

# Sample API documentation
We just use Markdown with special syntax to document our API.

# Group Users
A User is a representation of a User on the system.

## Users [/users{?cursor}]
Endpoint for users.

### List Users [GET]
Returns a list of users paginated by the cursor.

+ Parameters
    + cursor: `10` (number, optional) - Cursor value to paginate response.

+ Request (application/json)
    + Headers

            Accept: application/vnd.api-test.v1+json

+ Response 200 (application/json)

    + Attributes
        + data (array[User], fixed-type) - Users data.
        + pagination (object, required) - Pagination information.
            + cursors (object, required) - Cursors.
                + after: `10` (number, required) - Cursor for next record to fetch.
                + next_uri: `/users?cursor=5` (string, required) - URI for next page.
        + links (array, fixed-type, required) - Links references.
            + (object)
                + rel: `self` (string, required)
                + uri: `/users` (string, required)

### Create User [POST]
Creates a new User.

+ Request (application/json)
    + Headers

            Accept: application/vnd.api-test.v1+json

    + Attributes (User Base)

+ Response 201 (application/json)

    + Attributes
        + data (array[User], fixed-type) - Users data.
        + links (array, fixed-type, required) - Links references.
            + (object)
                + rel: `self` (string, required)
                + uri: `/users/1` (string, required)

+ Response 422 (application/json)

    + Attributes
        + data (array[User Base], fixed-type) - Users data.
        + errors (array, fixed-type, required) - Action error.
            + (object)
                + type: `blank` (string, required) - Type of validation error.
                + code: `ERR-100` (string, required) - Error unique code.
                + message: `email can't be blank.` (string, required) - Error descriptive message.
                + href: `http://api-test.io/api/documentation` (string, required)
                  Link to learn more about the error.



# Data Structures

# User Base (object)
+ email: `user@mail.com` (string) - User's email.
+ first_name: `Jane` (string, required) - User's first name.
+ last_name: `Doe` (string, required) - User's last name.

# User (User Base)
+ id (number) - User's ID.

