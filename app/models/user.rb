class User < ApplicationRecord
  validates :email, :first_name, :last_name, presence: true

  def as_json(options = {})
    json = {
      id: id,
      first_name: first_name || '',
      last_name: last_name || '',
      email: email || ''
    }

    json.delete(:id) if id.nil?
    json
  end
end
