# frozen_string_literal: true

class UsersController < ActionController::API
  def index
    users = User.all

    data = build_data_and_links(users)
           .merge(build_pagination(users))

    render json: data
  end

  def create
    user = User.new(user_params)

    if user.save
      data = build_data_and_links(user)
      status = :created
    else
      data = build_data_and_errors(user)
      status = :unprocessable_entity
    end

    render json: data, status: status
  end

  private

  def build_data_and_errors(resource)
    field, message = resource.errors.first
    error_type = resource.errors.details[field].first[:error]

    error_hash = {
      code: "#{field}_#{error_type}",
      type: error_type.to_s,
      message: message,
      href: 'http://api-test.io/api/documentation'
    }

    build_data(resource).merge(
      errors: [error_hash]
    )
  end

  def build_data_and_links(resources)
    build_data(resources).merge(
      links: [
        rel: 'self',
        uri: request.path
      ]
    )
  end

  def build_data(resources)
    {
      data: Array(resources)
    }
  end

  def build_pagination(resources)
    cursor = Array(resources).last.id
    {
      pagination: {
        cursors: {
          after: cursor,
          next_uri: users_path(cursor: cursor)
        }
      }
    }
  end

  def user_params
    params.permit(:email, :first_name, :last_name)
  end
end
