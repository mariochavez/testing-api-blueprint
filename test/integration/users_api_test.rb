# frozen_string_literal: true

require 'test_helper'

class UsersApiTest < ActionDispatch::IntegrationTest
  test 'Users List' do
    get '/users', headers: { Accept: 'application/vnd.api-test.v1+json' }

    assert_response :success
    assert_matches_json_schema response, 'GET-Users-200'
  end

  test 'Create new User successfully' do
    post '/users', headers: { Accept: 'application/vnd.api-test.v1+json' }, params: user_payload

    assert_response :created
    assert_matches_json_schema response, 'POST-Users-201'
  end

  test 'Fails to create new User' do
    post '/users', headers: { Accept: 'application/vnd.api-test.v1+json' },
                   params: user_payload(email: nil, first_name: nil)

    assert_response :unprocessable_entity
    assert_matches_json_schema response, 'POST-Users-422'
  end

  def user_payload(attrs = {})
    {
      email: 'user@mail.com',
      first_name: 'Jane',
      last_name: 'Doe'
    }.merge(attrs)
  end
end
