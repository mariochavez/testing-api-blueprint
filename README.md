# README

This is a sample Rails application on how to use API Blueprint to generate documentation and JSON schemas to be used
with js_matchers gem to verify that you API responds as your documentation says.

## See generated documentation
```
$ bundle
$ bin/yarn install
$ bin/rails api:documentation
$ bin/rails s
```
Point your browser to http://localhost:3000

## Run tests
```
$ bin/rails api:schemas
$ bin/rails test
```
